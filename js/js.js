let id = 1;
let object = {};
object.counter = 0;

function controller(model,view) {
    this.init = function () {
        const storesList = view.getStoresList();
        const searchBtn = view.getBtnSearch();
        const resetBtn = view.getBtnReset();
        const deleteProduct = view.deleteProduct();
        const editProduct = view.deleteProduct();
        this.openAddProduct();
        this.createStore();
        this.closeCreateShop();
        this.loadAllStores();
        this.closeAddProduct();
        this.closeCreateShop();
        this.deleteStore();
        this.closeDeleteStore();
        this.btnCreateNewStore();
        this.btnCreateNewProduct();
        this.btnEditProduct();
        this.closeEditForm();
        this.deleteStoreInList();
        this.sortingByName();
        this.sortingByPrice();
        this.sortingBySpecs();
        this.sortingBySupplier();
        this.sortingByCountry();
        this.sortingByProd();
        this.sortingByRating();
        view.getSearchInput().addEventListener("keyup",this.pressEnter);
        editProduct.addEventListener("click",this.openEditForm.bind(this));
        storesList.addEventListener("click", this.onStoreClicked.bind(this));
        searchBtn.addEventListener("click", this.onBtnSearchClick.bind(this));
        resetBtn.addEventListener("click", this.onBtnResetClick.bind(this));
        deleteProduct.addEventListener("click", this.deleteProductInList.bind(this));
        const contactInfo = view.createContactInfo();
    };
    this.loadData = function () {
        let elem = document.querySelector("#preloader");
        elem.style = "display:block";
        setTimeout(function () {
            elem.style = "display:none";
        }, 2000);
    };
    this.onStoreClicked = function (event) {
        const target = event.target.closest(".aside__list--content");
        object.id = target.id;
        const element = view.getAsideList();
        /*let element = document.querySelectorAll(".aside__list--content");*/
        for (let i = 0; i < element.length; i++) {
            element[i].classList.remove("active");
        }
        target.classList.add("active");
        let elem = view.noToChoose();
        elem.style.display = "none";
        this.loadContacts();
        this.counterProducts();
        this.loadAllProducts();
        this.loadData();
    };
    this.sortingByName = function () {
        let elem = view.sortingName();
        elem.onclick = function () {
            object.sorting = "Name";


            elem.children[0].classList.add("visible-none");
            if (elem.children[1].classList.toggle('change-sorting')) {
                elem.children[1].classList.remove("change-sorting");
            }
        };
        elem.addEventListener("click",this.sortingFunc);
    };
    this.sortingByPrice = () => {
        let elem = view.sortingPrice();
        elem.onclick = function () {
            object.sorting = "Price";

            let soska = document.querySelector('.content__header--title');
            for (let i = 0; i <= soska.children.length; i++) {
                if (soska.children[i].children[0].classList.contains('visible-none')) {
                    document.querySelector('.content__header--title').children[i].children[0].classList.remove('visible-none');
                    document.querySelector('.content__header--title').children[i].children[1].classList.add('visible-none');

                }
            }

        };

        elem.addEventListener("click",this.sortingFunc);
    };
    this.sortingBySpecs = () => {
        let elem = view.sortingSpecs();
        elem.onclick = function () {
            object.sorting = "Specs";
        };
        elem.addEventListener("click",this.sortingFunc);
    };
    this.sortingBySupplier = () => {
        let elem = view.sortingSupplier();
        elem.onclick = function () {
            object.sorting = "Supplier";
        };
        elem.addEventListener("click",this.sortingFunc);
    };
    this.sortingByCountry = () => {
        let elem = view.sortingCountry();
        elem.onclick = function () {
            object.sorting = "MadeIn";
        };
        elem.addEventListener("click",this.sortingFunc);
    };
    this.sortingByProd = () => {
        let elem = view.sortingProd();
        elem.onclick = function () {
            object.sorting = "ProductionCompanyName";
        };
        elem.addEventListener("click",this.sortingFunc);
    };
    this.sortingByRating = () => {
        let elem = view.sortingRating();
        elem.onclick = function () {
            object.sorting = "Rating";
        };
        elem.addEventListener("click",this.sortingFunc);
    };
    this.sortingFunc = function () {
        object.counter++;
        console.log(object.counter);
        if (object.counter % 2 == 0) {
            object.typeSorting = "DESC";
        }
        else {
            object.typeSorting = "ASC";
        }
        const sorting = model
            .setSortingType()
            .then(res => JSON.parse(res))
            .then(data => view.productList(data))
            .then(this.deleteProductInList)
            .then(this.openEditForm);
    };
    this.pressEnter = event => {
        if (event.keyCode == 13) {
            const element = view.getAside();
            element.innerHTML = "";
            let target = document.querySelector("#text-to-find");
            let search = target.value;
            this.loadAllStores(search.toLowerCase());
        }
    };
    this.onBtnSearchClick = function (elem) {
        localStorage.setItem('search', elem.value);
        const element = view.getAside();
        element.innerHTML = "";
        let target = document.querySelector("#text-to-find");
        let search = target.value;
        this.loadAllStores(search.toLowerCase());
    };
    this.onBtnResetClick = function () {
        localStorage.setItem("search", "");
        this.loadAllStores();
    };

    this.createStore = function () {
        const q = view.getCreateBtnAside();
        q.onclick = function () {
            let elem = document.querySelector(".shop-add");
            elem.style.display = "block"
        }
    };

    this.closeCreateShop = function () {
        let q = document.querySelector(".cancel-button--store");
        q.onclick = function () {
            const elem = view.getShopAdd();
            elem.style.display = "none";
        }
    };

    this.openAddProduct = function () {
        let elem = view.getBtnCreateProduct();
        elem.onclick = function () {
            let element = view.getFormProductAdd();
            element.style = "display:block";
        }
    };

    this.closeAddProduct = function () {
        let elem = document.querySelector(".cancel-button");
        elem.onclick = function () {
            let element = document.querySelector(".product-add");
            element.style.display = "none"
        }
    };

    this.closeEditForm = function () {
      let elem = document.querySelector(".edit-cancel-button--product");
      elem.onclick = function () {
          let element = document.querySelector(".product-edit");
          element.style.display = "none";
      }
    };
    this.deleteStore = function () {
        let elem = document.querySelector(".right-footer__footer--Delete");
        elem.onclick = function () {
            let element = document.querySelector(".sure-delete");
            element.style = "display:flex";
        }
    };

    this.closeDeleteStore = function () {
        let elem = document.querySelector(".delete-store-btn");
        elem.onclick = function () {
            let element = document.querySelector(".sure-delete");
            element.style = "display:none";
        }
    };

    this.checkAddStoreFormEmail = () => {
        let elem = view.getInputEmail();
        elem.style = "border: 1px solid black";
        if (/^[a-zA-Z-.]+@[a-z]+\.[a-z]{2,3}$/.test(`${elem.value}`)){
            this.checkAddStoreFormPhone();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkAddStoreFormPhone = () => {
        let elem = view.getInputPhoneNumber();
        elem.style = "border: 1px solid black";
        if (elem.value !== "") {
            this.checkAddStoreFormAddress();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkAddStoreFormAddress = () => {
        let elem = view.getInputAddress();
        elem.style = "border: 1px solid black";
        if ( /^[A-zA-z0-9,\.\s]+$/.test(`${elem.value}`)){
           this.checkAddStoreFormDate()
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkAddStoreFormDate = () => {
        let elem = view.getInputDate();
        elem.style = "border: 1px solid black";
        if (elem.value !== ""){
            this.getInputFloorArea();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.getInputFloorArea = () => {
        let elem = view.getInputFloor();
        elem.style = "border: 1px solid black";
        if (/^\d+$/.test(`${elem.value}`)){
            const addStore = model
                .createNewStoreUrl();
            setTimeout(function () {
                const test = model
                    .getStoresList()
                    .then(res => JSON.parse(res))
                    .then(stores => view.createStoresList(stores));
            }, 50);
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkAddStoreFormName = () => {
        let elem =  view.getInputName();
        elem.style = "border: 1px solid black";
        if (elem.value !== "") {
            this.checkAddStoreFormEmail()
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkAddProductFormName = () => {
        let elem = view.getInputProductName();
        if (elem.value !== "") {
            this.checkAddProductFormPrice()
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkAddProductFormPrice = () => {
        let elem = view.getInputProductPrice();
        if (elem.value !== "") {
            this.checkAddProductFormSpecs();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkAddProductFormSpecs = () => {
        let elem = view.getInputProductSpecs();
        if (elem.value !== "") {
            this.checkAddProductFormRating();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkAddProductFormRating = () => {
        let elem = view.getInputProductRating();
        if (/^\d+$/.test(`${elem.value}`) && Number(elem.value) <= 5 && Number(elem.value) >= 1) {
            this.checkAddProductFormSupplier();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkAddProductFormSupplier = () => {
        let elem = view.getInputProductSupplier();
        if (elem.value !== "") {
            this.checkAddProductFormCountry();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkAddProductFormCountry = () => {
        let elem = view.getInputProductCountry();
        if (/^[A-Za-z]+$/.test(`${elem.value}`)) {
            this.checkAddProductFormProd();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkAddProductFormProd = () => {
        let elem = view.getInputProductProd();
        if (/^[A-Za-z]+$/.test(`${elem.value}`)) {
            this.checkAddProductFormStatus();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkAddProductFormStatus = () => {
        let elem = view.getInputProductStatus();
        if (/^[A-Za-z]+$/.test(`${elem.value}`)) {
            const addProduct = model
                .createProduct();
            setTimeout(function () {
                const refreshProducts = model
                    .getStoresProducts()
                    .then(res => JSON.parse(res))
                    .then(data => view.productList(data))
            }, 30);
            setTimeout(function () {
                const refreshProducts = model
                    .getStoresProducts()
                    .then(res => JSON.parse(res))
                    .then(data=>view.sorting(data))
            },30);
            this.okClick();
            this.storageClick();
            this.outClick();
            this.allClick();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };

    this.checkEditProductFormName = () => {
        let elem = view.getInputEditProductName();
        if (elem.value !== "") {
            this.checkEditProductFormPrice()
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkEditProductFormPrice = () => {
        let elem = view.getInputEditProductPrice();
        if (elem.value !== "") {
            this.checkEditProductFormSpecs();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkEditProductFormSpecs = () => {
        let elem = view.getInputEditProductSpecs();
        if (elem.value !== "") {
            this.checkEditProductFormRating();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkEditProductFormRating = () => {
        let elem = view.getInputEditProductRating();
        if (/^\d+$/.test(`${elem.value}`) && Number(elem.value) <= 5 && Number(elem.value) >= 1) {
            this.checkEditProductFormSupplier();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkEditProductFormSupplier = () => {
        let elem = view.getInputEditProductSupplier();
        if (elem.value !== "") {
            this.checkEditProductFormCountry();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkEditProductFormCountry = () => {
        let elem = view.getInputEditProductCountry();
        if (/^[A-Za-z]+$/.test(`${elem.value}`)) {
            this.checkEditProductFormProd();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkEditProductFormProd = () => {
        let elem = view.getInputEditProductProd();
        if (/^[A-Za-z]+$/.test(`${elem.value}`)) {
            this.checkEditProductFormStatus();
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };
    this.checkEditProductFormStatus = () => {
        let elem = view.getInputEditProductStatus();
        if (/^[A-Za-z]+$/.test(`${elem.value}`)) {
            const editProduct = model
                .updateProduct();
            setTimeout(function () {
                const refreshProducts = model
                    .getStoresProducts()
                    .then(res => JSON.parse(res))
                    .then(data => view.productList(data));
            }, 30);
            let element = document.querySelector(".product-edit");
            element.style.display = "none";
        }
        else {
            elem.style = "border: 1px solid red";
        }
    };

    this.btnCreateNewStore = function () {
        let elem = document.querySelector(".create-button");
        elem.addEventListener("click",this.checkAddStoreFormName);
    };
    this.btnCreateNewProduct = function () {
        let elem = document.querySelector(".create-button--product");
        elem.addEventListener("click",this.checkAddProductFormName);
    };

    this.btnEditProduct = function () {
        let elem = document.querySelector(".edit-button--product");
        elem.addEventListener("click",this.checkEditProductFormName);

    };
    this.allClick = function () {
        let elem = document.querySelector(".all-products");
        let capture = 0;
        elem.onclick = function () {
            const query = model
                .getStoresProducts()
                .then(res => JSON.parse(res))
                .then(data => view.sortingBtn(data, capture));
        }
    };

    this.okClick = function () {
        let elem = document.querySelector(".ok-products");
        let capture = 1;
        elem.onclick = function () {
            const query = model
                .getStoresProducts()
                .then(res => JSON.parse(res))
                .then(data => view.sortingBtn(data, capture));
        }
    };

    this.storageClick = function () {
        let elem = document.querySelector(".storage-products");
        let capture = 2;
        elem.onclick = function () {
            const query = model
                .getStoresProducts()
                .then(res => JSON.parse(res))
                .then(data => view.sortingBtn(data, capture));
        }
    };

    this.outClick = function () {
        let elem = document.querySelector(".out-products");
        let capture = 3;
        elem.onclick = function () {
            const query = model
                .getStoresProducts()
                .then(res => JSON.parse(res))
                .then(data => view.sortingBtn(data, capture));
        }
    };

    this.loadContacts = function () {
        const contacts = model
            .getStoresList()
            .then(res => JSON.parse(res))
            .then(stores => view.createContactInfo(stores));
    };
    this.loadAllStores = function (search) {
        const allStores = model
            .getStoresList()
            .then(res => JSON.parse(res))
            .then(stores => view.createStoresList(stores, search))
            .then(this.pressEnter)
    };
    this.counterProducts = function () {
        const counter = model
            .getStoresProducts()
            .then(res => JSON.parse(res))
            .then(data => view.sorting(data))
            .then(this.okClick)
            .then(this.storageClick)
            .then(this.outClick)
            .then(this.allClick)
        //.then(this.deleteProductInList)
    };
    this.loadAllProducts = function () {
        const allProducts = model
            .getStoresProducts()
            .then(res => JSON.parse(res))
            .then(data => view.productList(data))
            .then(this.deleteProductInList)
            .then(this.openEditForm)
    };
    this.deleteProductInList = function (elem) {
        const target = elem.target.closest(".content__list");
        object.deleteProduct = target.id;
        target.children[8].addEventListener("click",this.wasClicked.bind(this));
    };
    this.wasClicked = function () {
        model.deleteThisProduct();
        this.loadAllProducts()
    };
    this.openEditForm = function (elem) {
        const target = elem.target.closest(".content__list");
        object.deleteProduct = target.id;
        target.children[7].addEventListener("click",this.openForm);
    };

    this.openForm = function () {
        let elem = document.querySelector(".product-edit");
        object.edit = this.id;
        elem.style.display = "block";
    };

    this.deleteStoreInList = function () {
        let elem = document.querySelector(".btn-primary");
        elem.onclick = function () {
            let element = document.querySelector(".sure-delete");
            element.style.display = "none";
            const deleteStore = model
                .deleteStore();
            setTimeout(function () {
            const refreshStores = model
                    .getStoresList()
                    .then(res=>JSON.parse(res))
                    .then(stores=>view.createStoresList(stores));
            },200)
        }
    };
}

function model() {

    /**
     *make a request for all stores
     * @param url
     * @returns {Promise}
     */
    this.getData = function (url) {
        return new Promise((resolve,reject)=>{
            const req = new XMLHttpRequest();
            req.open("GET",url,true);
            req.addEventListener("load",function (){
                if (req.status === 200) {
                    resolve(req.response);
                } else {
                    const error = new Error (req.statusText);
                    error.code = req.status;
                    reject(error);
                }
            }) ;
            req.addEventListener("error",()=> reject(new Error("network Error")));
            req.send();
        })
    };

    /**
     * remove the product from the list
     * @param url
     * @returns {Promise}
     */
    this.deleteProductInList = function (url) {
        return new Promise((resolve,reject)=>{
            let xhr = new XMLHttpRequest();
            xhr.open("DELETE", url, false);
            xhr.addEventListener("load",function (){
                if (xhr.status === 200) {
                    resolve(xhr.response);
                } else {
                    const error = new Error (xhr.statusText);
                    error.code = xhr.status;
                    reject(error);
                }
            }) ;
            xhr.addEventListener("error",()=> reject(new Error("network Error")));
            xhr.send(null);
        })
    };

    /**
     * determine url to get a list of stores
     * @returns {Promise}
     */
    this.getStoresList = function() {
        return this.getData(`http://localhost:3000/api/Stores`);
    };
    /**
     * determine url to delete a product from list
     * @returns {Object}
     */
    this.deleteThisProduct = function () {
        return this.deleteProductInList(`http://localhost:3000/api/Products/${object.deleteProduct}`);
    };
    /**
     *make a request for all stores
     * @param url
     * @returns {Promise}
     */
    this.getProducts = function (url) {
        return new Promise((resolve,reject)=>{
            const req = new XMLHttpRequest();
            req.open("GET",url,true);
            req.addEventListener("load",function (){
                if (req.status === 200) {
                    resolve(req.response);
                } else {
                    const error = new Error (req.statusText);
                    error.code = req.status;
                    reject(error);
                }
            }) ;
            req.addEventListener("error",()=> reject(new Error("network Error")));
            req.send();

        })
    };
    /**
     * determine url to get a list of products
     * @returns {Object}
     */
    this.getStoresProducts = function() {
        return this.getProducts(`http://localhost:3000/api/Stores/${object.id}/rel_Products`);
    };
    /**
     * Request to add a new store to List
     * @param url
     * @returns {Promise}
     */
    this.createNewStore = function (url) {
        id = 1;
        let sendBtn = document.querySelector(".form-shop");
        let formData = new FormData(sendBtn);
        let obj = {};
        formData.forEach((elem,key)=>{
            obj[key] = elem;
        });
        fetch(url, {
            method: 'POST', // или 'PUT'
            body: JSON.stringify(obj), // data может быть типа `string` или {object}!
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(res => res.json());

        object.capture = 1;
        let elem = document.querySelector('.shop-add');
        elem.style = "display:none";
    };
    /**
     * determine url to add a store to list
     * returns {Object}
     */
    this.createNewStoreUrl = function () {
        return this.createNewStore("http://localhost:3000/api/Stores?access_token=product");
    };
    /**
     * Request to add a new product to List
     * @param url
     * returns {Promise}
     */
    this.createNewProduct = function (url) {
        let storeId = object.id;
        let sendBtn = document.querySelector(".form-product");
        let formData = new FormData(sendBtn);
        let caption = 0;
        let obj = {};
        formData.forEach((elem,key)=>{
            obj[key] = elem;
            obj.StoreId = storeId;
            obj.Photo = "-";
            obj.Status = elem.toUpperCase()
        });
        fetch(url, {
            method: 'POST',
            body: JSON.stringify(obj),
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(res => res.json());
        let elem = document.querySelector(".product-add");
        elem.style = "display:none";
    };
    /**
     * determine url to add a product to list
     * returns {Object}
     */
    this.createProduct = function () {
        return this.createNewProduct("http://localhost:3000/api/Products");
    };
    /**
     * Request to delete store from list
     * @param url
     */
    this.deleteThisStore = function (url) {
        let xhr = new XMLHttpRequest();
        xhr.open("DELETE", url, true);
        xhr.send(null);
    };
    /**
     * determine url to delete a store from list
     * returns {Object}
     */
    this.deleteStore = function () {
        return this.deleteThisStore(`http://localhost:3000/api/Stores/${object.id}`);
    };
    /**
     * Request to edit product in list
     * @param url
     * returns {Promise}
     */
    this.updateThisProduct = function (url) {
        let storeId = object.id;
        let sendBtn = document.querySelector(".form-product-edit");
        let formData = new FormData(sendBtn);
        let caption = 0;
        let obj = {};
        formData.forEach((elem,key)=>{
            obj[key] = elem;
            obj.StoreId = storeId;
            obj.Photo = "-";
            //obj.id = object.deleteProduct;
        });
        obj.id = object.deleteProduct;
        fetch(url, {
            method: 'PATCH',
            body: JSON.stringify(obj),
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(res => res.json());
        let elem = document.querySelector(".product-edit");
        //elem.style.display = "none";
    };
    /**
     * determine url to update a product in list
     */
    this.updateProduct = function () {
        return this.updateThisProduct(`http://localhost:3000/api/Products/${object.deleteProduct}`)
    };
    /**
     * Request to get a data to sorting
     * @param url
     * @returns {Promise}
     */
    this.sortingData = function (url) {
        return new Promise((resolve,reject)=>{
            const req = new XMLHttpRequest();
            req.open("GET",url,true);
            req.addEventListener("load",function (){
                if (req.status === 200) {
                    resolve(req.response);
                } else {
                    const error = new Error (req.statusText);
                    error.code = req.status;
                    reject(error);
                }
            }) ;
            req.addEventListener("error",()=> reject(new Error("network Error")));
            req.send();
        })
    };
    /**
     * determine url to sorting a products in list
     * @returns {Promise}
     */
    this.setSortingType = () => {
       return this.sortingData(`http://localhost:3000/api/Stores/${object.id}/rel_Products?filter[order]=${object.sorting} ${object.typeSorting}`);
    }
}

function view() {
    /**
     * get search input
     * @returns {Element}
     */

    this.getSearchInput = () => document.querySelector("#text-to-find");
    /**
     * get a message that nothing is selected
     * @returns {Element}
     */
    this.noToChoose = () => document.querySelector(".no-to-choose");
    /**
     * get a column Name for sorting
     * @returns {Element}
     */
    this.sortingName =  () => document.querySelector(".column-name");
    /**
     * get a column Price for sorting
     * @returns {Element}
     */
    this.sortingPrice = () => document.querySelector(".column-price");
    /**
     * get a column Specs for sorting
     * @returns {Element}
     */
    this.sortingSpecs = () => document.querySelector(".column-specs");
    /**
     * get a column Supplier for sorting
     * @returns {Element}
     */
    this.sortingSupplier = () => document.querySelector(".column-supplier");
    /**
     * get a column Country for sorting
     * @returns {Element}
     */
    this.sortingCountry = () => document.querySelector(".column-country");
    /**
     * get a column Prod. for sorting
     * @returns {Element}
     */
    this.sortingProd = () => document.querySelector(".column-prod");
    /**
     * get a column Rating for sorting
     * @returns {Element}
     */
    this.sortingRating = () => document.querySelector(".column-rating");
    /**
     * get a div whose contains the store
     * @returns {NodeListOf<Element>}
     */
    this.getAsideList = () => document.querySelectorAll(".aside__list--content");
    /**
     *get a div whose contains the all stores
     * @returns {Element}
     */
    this.getAside = () => document.querySelector(".aside__list");
    /**
     * get a div whose contains the button for create a new store
     * @returns {Element}
     */
    this.getCreateBtnAside = () => document.querySelector(".create-button-aside");
    /**
     * get a div whose contains the form to add the store
     * @returns {Element}
     */
    this.getShopAdd = () => document.querySelector(".shop-add");
    /**
     * get a div whose contains the button for create a new product
     * @returns {Element}
     */
    this.getBtnCreateProduct = () => document.querySelector(".right-footer__footer--Create");
    /**
     * get a div whose contains the form to create a new product
     * @returns {Element}
     */
    this.getFormProductAdd = () => document.querySelector(".product-add");
    /**
     * get a div whose contains the input with email
     * @returns {Element}
     */
    this.getInputEmail = () => document.querySelector(".input-email");
    /**
     * get a div whose contains the input with name
     * @returns {Element}
     */
    this.getInputName = () => document.querySelector(".input-name");
    /**
     * get a div whose contains the input with phone
     * @returns {Element}
     */
    this.getInputPhoneNumber = () => document.querySelector(".input-phone");
    /**
     * get a div whose contains the input with address
     * @returns {Element}
     */
    this.getInputAddress = () => document.querySelector(".input-address");
    /**
     * get a div whose contains the input with established date
     * @returns {Element}
     */
    this.getInputDate = () => document.querySelector(".input-est");
    /**
     * get a div whose contains the input with floor area
     * @returns {Element}
     */
    this.getInputFloor = () => document.querySelector(".input-floor");
    /**
     * get a div whose contains the input with name to add a new product
     * @returns {Element}
     */
    this.getInputProductName = () => document.querySelector(".enter-name");
    /**
     * get a div whose contains the input with price to add a new product
     * @returns {Element}
     */
    this.getInputProductPrice = () => document.querySelector(".enter-price");
    /**
     * get a div whose contains the input with specs to add a new product
     * @returns {Element}
     */
    this.getInputProductSpecs = () => document.querySelector(".enter-specs");
    /**
     * get a div whose contains the input with rating to add a new product
     * @returns {Element}
     */
    this.getInputProductRating = () => document.querySelector(".enter-rating");
    /**
     * get a div whose contains the input with supplier to add a new product
     * @returns {Element}
     */
    this.getInputProductSupplier = () => document.querySelector(".enter-supplier");
    /**
     * get a div whose contains the input with country to add a new product
     * @returns {Element}
     */
    this.getInputProductCountry = () => document.querySelector(".enter-country");
    /**
     * get a div whose contains the input with prod to add a new product
     * @returns {Element}
     */
    this.getInputProductProd = () => document.querySelector(".enter-prod");
    /**
     * get a div whose contains the input with status to add a new product
     * @returns {Element}
     */
    this.getInputProductStatus = () => document.querySelector(".enter-status");
    /**
     * get a div whose contains the input with name to edit a product
     * @returns {Element}
     */
    this.getInputEditProductName = () => document.querySelector(".edit-name");
    /**
     * get a div whose contains the input with price to edit a product
     * @returns {Element}
     */
    this.getInputEditProductPrice = () => document.querySelector(".edit-price");
    /**
     * get a div whose contains the input with specs to edit a product
     * @returns {Element}
     */
    this.getInputEditProductSpecs = () => document.querySelector(".edit-specs");
    /**
     * get a div whose contains the input with rating to edit a product
     * @returns {Element}
     */
    this.getInputEditProductRating = () => document.querySelector(".edit-rating");
    /**
     * get a div whose contains the input with supplier to edit a product
     * @returns {Element}
     */
    this.getInputEditProductSupplier = () => document.querySelector(".edit-supplier");
    /**
     * get a div whose contains the input with country to edit a product
     * @returns {Element}
     */
    this.getInputEditProductCountry = () => document.querySelector(".edit-country");
    /**
     * get a div whose contains the input with prod. to edit a product
     * @returns {Element}
     */
    this.getInputEditProductProd = () => document.querySelector(".edit-prod");
    /**
     * get a div whose contains the input with status to edit a product
     * @returns {Element}
     */
    this.getInputEditProductStatus = () => document.querySelector(".edit-status");
    /**
     * get a button to search in stores list
     * @returns {Element}
     */
    this.getBtnSearch = () => document.querySelector(".search");
    /**
     * get a button to cancel the search
     * @returns {Element}
     */
    this.getBtnReset = () => document.querySelector(".cancel");
    /**
     * get a div whose contains a list with products
     * @returns {Element}
     */
    this.deleteProduct = () => document.querySelector(".list-right");
    /**
     * get a div whose contains a list with stores
     * @returns {Element}
     */
    this.getStoresList = () => document.querySelector(".aside__list");
    /**
     * check conditions before to create a stores list after create a search request
     * @param data
     * @param search
     */
    this.createStoresList = function (data,search) {
        let asideList = document.querySelector(".aside__list");
        asideList.innerHTML = "";
        if (search) {
            let counter = 0;
            for (let i = 0; i < data.length; i++) {
                let floorArea = String(data[i].FloorArea);
                if (data[i].Name.toLowerCase().indexOf(search) != -1
                    || data[i].Address.toLowerCase().indexOf(search) != -1
                    || floorArea.indexOf(search) != -1) {
                    counter++;
                    this.shopFormation(i, data);
                }
            }
        } else {
            for (let i = 0; i < data.length; i++) {
                this.shopFormation(i, data);
            }
        }
    };
    /**
     * Create stores list
     * @param i
     * @param data
     */
    this.shopFormation = function (i,data) {
            let asideList = document.querySelector(".aside__list");
            let block = document.createElement("section");
            let childBlock = document.createElement("section");
            let paragraph = document.createElement("p");
            let numberParagraph = document.createElement("p");
            let span = document.createElement("span");
            block.id = data[i].id;
            id++;
            block.type = "button";
            this.createClass(block,paragraph,numberParagraph,childBlock);
            this.addInnerHtml(paragraph,numberParagraph,span,i,data);
            this.appendChild(asideList,block,paragraph,numberParagraph,childBlock,span);
    };
    /**
     * Add the classnames to elements
     * @param block
     * @param paragraph
     * @param numberParagraph
     * @param childBlock
     */
    this.createClass = function (block,paragraph,numberParagraph,childBlock) {
        block.className = "aside__list--content";
        paragraph.className = "name";
        numberParagraph.className = "number";
        childBlock.className = "aside__list--content_p";
    };
    /**
     * add InnerHTML to elements
     * @param paragraph
     * @param numberParagraph
     * @param span
     * @param i
     * @param data
     */
    this.addInnerHtml = function (paragraph,numberParagraph,span,i,data) {
        paragraph.innerHTML = data[i].Name;
        numberParagraph.innerHTML = data[i].FloorArea;
        span.innerHTML = data[i].Address;
    };
    /**
     * Append childs to our elements
     * @param asideList
     * @param block
     * @param paragraph
     * @param numberParagraph
     * @param childBlock
     * @param span
     */
    this.appendChild = function (asideList,block,paragraph,numberParagraph,childBlock,span) {
        childBlock.appendChild(paragraph);
        childBlock.appendChild(numberParagraph);
        block.appendChild(childBlock);
        asideList.appendChild(block);
        block.appendChild(span);
    };
    /**
     * Create a contact info
     * @param data
     */
    this.createContactInfo = function (data) {
        let email = document.querySelector(".email");
        let phone = document.querySelector(".phone");
        let address = document.querySelector(".address");
        let date = document.querySelector(".date");
        let floor = document.querySelector(".floor");
        for (let i = 0; i < data.length; i++){
            if (object.id == data[i].id){
                email.innerHTML = "<strong>" + "Email: " + "</strong>"+ data[i].Email;
                phone.innerHTML = "<strong>" + "Phone number: " + "</strong>" + data[i].PhoneNumber;
                address.innerHTML = "<strong>" + "Address: " + "</strong>" + data[i].Address;
                date.innerHTML = "<strong>" + "Established Date: " + "</strong>" + data[i].Established;
                floor.innerHTML = "<strong>" + "Floor Area: " + "</strong>" + data[i].FloorArea;
            }
        }
    };
    /**
     * Clear a list of products
     * @param data
     */
    this.productList = function (data) {
        let list = document.querySelector(".list-right");
        list.innerHTML = "";
        for(let i = 0; i < data.length; i++){
            this.createProductList(data,i,list);
        }
    };
    /**
     * sorting by good products only
     * @param data
     * @param i
     * @param list
     */
    this.productListOkSorting = function(data,i,list) {
        this.createProductList(data,i,list);
    };
    /**
     * sorting by storage products only
     * @param data
     * @param i
     * @param list
     */
    this.productListStorageSorting = function (data,i,list) {
        this.createProductList(data,i,list);
    };
    /**
     * sorting by out_of_stock products only
     * @param data
     * @param i
     * @param list
     */
    this.productListOutSorting = function (data,i,list) {
        this.createProductList(data,i,list);
    };
    /**
     * Create our product list
     * @param data
     * @param i
     * @param list
     */
    this.createProductList = function (data,i,list) {
        let content = document.createElement("section");
        content.className = "content__list"; /* if remove this line to createClassName i get Error */
        content.id = data[i].id;
        let name = document.createElement("span");
        let price = document.createElement("span");
        let specs = document.createElement("span");
        let supplier = document.createElement("span");
        let country = document.createElement("span");
        let prod = document.createElement("span");
        let rating = document.createElement("span");
        let deleteProduct = document.createElement("i");
        deleteProduct.id = data[i].id;
        let editProduct = document.createElement("i");
        editProduct.id = data[i].id;
        editProduct.style = "margin-left:-35px; padding-right: 8px";
        let counter = data[i].Rating;
        this.createClassName(name,price,specs,supplier,country,prod,rating,deleteProduct,editProduct);
        this.addInnerHtmlList(data,name,price,specs,supplier,country,prod,rating,i,counter);
        this.addToChild(content,name,price,specs,supplier,country,prod,rating,deleteProduct,list,editProduct);

    };
    /**
     *Create a list only with the right products
     * @param data
     * @param capture
     */
    this.sortingBtn = function (data,capture) {
        let list = document.querySelector(".list-right");
        list.innerHTML = "";
        for (let i = 0; i < data.length; i++){
            if (data[i].Status == "OK"&& capture == 1  ) {
                this.productListOkSorting(data,i,list);
            }
            else if (capture == 2 && data[i].Status == "STORAGE") {
                this.productListStorageSorting(data,i,list);
            }
            else if (capture == 3 && data[i].Status == "OUT_OF_STOCK") {
                this.productListOutSorting(data,i,list);
            }
            else if (capture == 0) {
                this.createProductList(data,i,list);
            }
        }
    };
    /**
     * Create classname table column names
     * @param name
     * @param price
     * @param specs
     * @param supplier
     * @param country
     * @param prod
     * @param rating
     * @param deleteProduct
     * @param editProduct
     */
    this.createClassName = function (name,price,specs,supplier,country,prod,rating,deleteProduct,editProduct) {
        name.className = "name";
        price.className = "price";
        specs.className = "specs";
        supplier.className = "supplier";
        country.className = "country";
        prod.className = "prod";
        rating.className = "rating";
        deleteProduct.className = "fas fa-ban";
        editProduct.className = "fas fa-pen work-pls";
    };
    /**
     * Add InnerHTML to our table column names
     * @param data
     * @param name
     * @param price
     * @param specs
     * @param supplier
     * @param country
     * @param prod
     * @param rating
     * @param i
     * @param counter
     */
    this.addInnerHtmlList = function (data,name,price,specs,supplier,country,prod,rating,i,counter) {
        name.innerHTML = data[i].Name;
        price.innerHTML = data[i].Price;
        specs.innerHTML = data[i].Specs;
        supplier.innerHTML = data[i].SupplierInfo;
        country.innerHTML = data[i].MadeIn;
        prod.innerHTML = data[i].ProductionCompanyName;
        for (let i = 0; i < counter; i++){
            rating.innerHTML += "<i class=\"fas fa-star\"></i>";
        }
    };
    /**
     * Append child to our table column names
     * @param content
     * @param name
     * @param price
     * @param specs
     * @param supplier
     * @param country
     * @param prod
     * @param rating
     * @param deleteProduct
     * @param list
     * @param editProduct
     */
    this.addToChild = function (content,name,price,specs,supplier,country,prod,rating,deleteProduct,list,editProduct) {
        content.appendChild(name);
        content.appendChild(price);
        content.appendChild(specs);
        content.appendChild(supplier);
        content.appendChild(country);
        content.appendChild(prod);
        content.appendChild(rating);
        content.appendChild(editProduct);
        content.appendChild(deleteProduct);
        list.appendChild(content);
    };
    /**
     * Count how many products of what quality and re-painting our list of products
     * @param data
     */
    this.sorting = function (data) {
        let counterOk = 0;
        let counterStorage = 0;
        let counterOut = 0;
        let counterProduct = 0;
        for (let i = 0; i < data.length; i++){
            if (data[i].Status == "OK"){
                counterOk++
            }
            else if(data[i].Status == "STORAGE") {
                counterStorage++
            }
            else if(data[i].Status == "OUT_OF_STOCK") {
                counterOut++;
            }
            counterProduct++;
        }
        let elem = document.querySelector(".header__counters");
        let allElements = document.createElement("div");
        let okProducts = document.createElement("div");
        let spanOk = document.createElement("span");
        let okImage = document.createElement("i");
        let classOk = document.createElement("div");
        let spanTitleOk = document.createElement("span");
        let storageProducts = document.createElement("div");
        let spanStorage = document.createElement("span");
        let storageImage = document.createElement("i");
        let classStorage = document.createElement("div");
        let spanTitleStorage = document.createElement("span");
        let outProducts = document.createElement("div");
        let spanOut = document.createElement("span");
        let outImage = document.createElement("i");
        let classOut = document.createElement("div");
        let spanTitleOut = document.createElement("span");
        this.addClassHeader(allElements,okProducts,okImage,classOk);
        this.addInnerHtmlHeader(elem,allElements,counterProduct,spanTitleOk,spanOk,counterOk);
        this.appendChildHeader(elem,allElements,okProducts,okImage,classOk,spanTitleOk,spanOk);
        this.addClassStorage(storageProducts,storageImage,classStorage);
        this.addInnerHtmlStorage(spanTitleStorage,spanStorage, counterStorage);
        this.appendChildStorage(elem,classStorage,storageImage,spanTitleStorage,storageProducts,spanStorage);
        this.addClassOut(outProducts,outImage,classOut);
        this.addInnerHtmlOut(spanTitleOut,spanOut, counterOut);
        this.appendChildOut(elem,classOut,outImage,spanTitleOut,outProducts,spanOut);
    };
    /**
     * Add classnames to ok elements
     * @param allElements
     * @param okProducts
     * @param okImage
     * @param classOk
     */
    this.addClassHeader = function (allElements,okProducts,okImage,classOk) {
        allElements.className = "all-products";
        okProducts.className = "ok-products";
        okImage.className = "fas fa-check-circle";
        classOk.className = "ok-title";
    };
    /**
     * Add InnerHtml to ok elements
     * @param elem
     * @param allElements
     * @param counterProduct
     * @param spanTitleOk
     * @param spanOk
     * @param counterOk
     */
    this.addInnerHtmlHeader = function (elem,allElements,counterProduct,spanTitleOk,spanOk,counterOk,) {
        elem.innerHTML = "";
        allElements.innerHTML = counterProduct + " All";
        spanTitleOk.innerHTML = "Ok";
        spanOk.innerHTML = counterOk;
    };
    /**
     * Add child elements to ok elements
     * @param elem
     * @param allElements
     * @param okProducts
     * @param okImage
     * @param classOk
     * @param spanTitleOk
     * @param spanOk
     */
    this.appendChildHeader = function (elem,allElements,okProducts,okImage,classOk,spanTitleOk,spanOk ) {
        classOk.appendChild(okImage);
        classOk.appendChild(spanTitleOk);
        okProducts.appendChild(classOk);
        okProducts.appendChild(spanOk);
        elem.appendChild(allElements);
        elem.appendChild(okProducts);
    };
    /**
     * Add class to storage elements
     * @param storageProducts
     * @param storageImage
     * @param classStorage
     */
    this.addClassStorage = function (storageProducts,storageImage,classStorage) {
        storageProducts.className = "storage-products";
        storageImage.className = "fas fa-exclamation-triangle";
        classStorage.className = "storage-title";
    };
    /**
     * add InnerHtml to storage elements
     * @param spanTitleStorage
     * @param spanStorage
     * @param counterStorage
     */
    this.addInnerHtmlStorage = function (spanTitleStorage,spanStorage, counterStorage) {
        spanTitleStorage.innerHTML = "Storage";
        spanStorage.innerHTML = counterStorage;
    };
    /**
     * add child elements to storage elements
     * @param elem
     * @param classStorage
     * @param storageImage
     * @param spanTitleStorage
     * @param storageProducts
     * @param spanStorage
     */
    this.appendChildStorage = function (elem,classStorage,storageImage,spanTitleStorage,storageProducts,spanStorage){
        classStorage.appendChild(storageImage);
        classStorage.appendChild(spanStorage);
        classStorage.appendChild(spanTitleStorage);
        storageProducts.appendChild(classStorage);
        storageProducts.appendChild(spanStorage);
        elem.appendChild(storageProducts);
    };
    /**
     * add classnames to out of stock elements
     * @param outProducts
     * @param outImage
     * @param classOut
     */
    this.addClassOut = function (outProducts,outImage,classOut) {
        outProducts.className = "out-products";
        outImage.className = "fas fa-exclamation-circle";
        classOut.className = "out-title";
    };
    /**
     * add InnerHtml to out of stock elements
     * @param spanTitleOut
     * @param spanOut
     * @param counterOut
     */
    this.addInnerHtmlOut = function (spanTitleOut,spanOut, counterOut) {
        spanTitleOut.innerHTML = "Out of stock";
        spanOut.innerHTML = counterOut;
    };
    /**
     * add child elements to out of stocks products
     * @param elem
     * @param classOut
     * @param outImage
     * @param spanTitleOut
     * @param outProducts
     * @param spanOut
     */
    this.appendChildOut = function (elem,classOut,outImage,spanTitleOut,outProducts,spanOut){
        classOut.appendChild(outImage);
        classOut.appendChild(spanOut);
        classOut.appendChild(spanTitleOut);
        outProducts.appendChild(classOut);
        outProducts.appendChild(spanOut);
        elem.appendChild(outProducts);
    };
}
new controller(new model(),new view()).init();